## MedUX

MedUX will be an [EHR](https://en.wikipedia.org/wiki/Electronic_health_record)
software for medical doctors (especially general practitioners),
physical therapists, non-medical practitioners and other persons in the
periphery of medicine. The software will be released under the AGPLv3,
therefore being completely open source, and can be used free of charge or
license by anyone.

If you feel encouraged helping out with programming, software architecture,
graphics, medical advices, sponsoring, etc., you are more than welcome - just
see the contact page. We speak German, English, and a bit Spanish.

Medux is in planning phase yet, there is no possibility getting running code by
now - but so far by now: It will most probably be implemented in Python. Albeit
mainly authored in Austria, all specifications are written in English to enable
interoperability and exchange with other countries. 

To find out more about this project, please visit the [Wiki](https://gitlab.com/nerdocs/medux/wikis/home).

## Why we do this

I tried and evaluated a lot of Medical software until now - all projects,
commercial or free, seemed either too
cumbersome, too buggy, or having a bad cost-performance ratio to me, so I
finally decided to start writing my own
implementation from scratch. MedUX is started as - let me say, a
proof-of-concept project, as many people tried to convince me that
this is impossible to do, Python is not a real language to program
large-scaled software in, or that the market is filledwith that kind of
applications.I don't care. If noone starts, it will never exist.

* MedUX aims to become a fully fledged Electronic Medical Record, in the first
line for the *Austrian* health system, based on Free Open Source software.
* It will be written mostly in Python, with a central server, and mainly a 
web frontend, written in JavaScript.
* Ideas/influences:
  * [FreeMedForms](www.freemedforms.com)
  * [Gnumed](www.gnumed.org)
  * and maybe other EMRs I have seen somewhere.


Feel free to fork this project, and/or help out with code, graphics, ideas,
translations and technical/medical feedback.
*Constructive* criticism (or even patches) are most welcome.


The software is licensed under the AGPLv3 which ensures that it will be
available to everyone who needs it without any dependency on proprietary
software. See more in the LICENSE file.

Aims of the project
===================

  * **Intuitive** - Easily learn using your EMR.
  * **Effective** - Software should be workflow acceleration.
  * **[Open Source]
 (https://gitlab.com/nerdoc/medux/wikis/why-open-from-the-start)** - Be
independent from your software vendor.
  * **Standards compliant** -  Keep up the interoperability with others.
  * **Cross platform** -  Windows, MacOS, Linux.
  * **Low system requirements** -  Don't be urged to update your hardware
  every 2 years.


About Me
=========

I am a medical doctor (GP) in Salzburg, Austria, and a hobby programmer,
interested and passionate about Free Software ("free" as in Freedom),
Open Source, privacy, cryptography, and software usability.



